package com.getinge.cpq.interfacemonitor.controllers;

import com.getinge.cpq.interfacemonitor.InterfaceMonitorApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InterfaceMonitorApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OnlinePricingControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port + "/CPQMonitorInterface/cpq/sales/onlinepricing";
    }

    @Test
    public void contextLoads() {
    }

   /* @Test
    public void testCreateRequest() {
        String str = "Test service Request";
        ResponseEntity<Request> postResponse = restTemplate.postForEntity(getRootUrl() + "/create", str, null, null, Request.class);
        Assert.assertNotNull(postResponse);
        Assert.assertNotNull(postResponse.getBody());
    }*/

}