package com.getinge.cpq.interfacemonitor.controllers;

import com.getinge.cpq.interfacemonitor.entities.TransferVO;
import com.getinge.cpq.interfacemonitor.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Online Pricing controller.
 * Used to receive the RestAPI calls from
 * Encoway's com.encoway.qa.server.sap.service.quotecreation package
 *
 * @author Khurram Islam
 */
@RestController
@RequestMapping("/cpq/sales")
public class QuoteToSAPController {

    @Autowired
    private RequestService requestService;

    private final String REQUEST_TYPE = "Submit quote to SAP";

    /**
     * RestAPI Post method to accept the call from CPQ-Embedded-Sales.
     *
     * @param transferVO - transfer object to frame request object.
     * @return ResponseEntity with the HTTP Status.
     */
    @PostMapping(path = "/quoteToSAP")
    public ResponseEntity<String> createRequest(@RequestBody TransferVO transferVO) {
        ResponseEntity<String> responseEntity = null;
        try {
            requestService.createRequestAndRequestEvent(transferVO, REQUEST_TYPE);
            responseEntity = new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception exp) {
            responseEntity = new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return responseEntity;
    }
}