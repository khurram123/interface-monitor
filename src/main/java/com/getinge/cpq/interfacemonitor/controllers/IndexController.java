package com.getinge.cpq.interfacemonitor.controllers;

import static javax.servlet.http.HttpServletResponse.SC_OK;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.getinge.cpq.interfacemonitor.configuration.Monitor;
import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import com.getinge.cpq.interfacemonitor.entities.RequestFilter;
import com.getinge.cpq.interfacemonitor.models.PagerModel;
import com.getinge.cpq.interfacemonitor.repositories.RequestRepository;
import com.getinge.cpq.interfacemonitor.services.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * CQPMonitorInterface index class.
 * All the processing on the main page happens from here.
 */
@Controller
public class IndexController {

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    private Monitor monitor;

    @Autowired
    private RequestService requestService;

    private Logger log = LoggerFactory.getLogger(IndexController.class);

    /**
     * Get all records/filter records on the index page.
     *
     * @param model - used for PageModel
     * @param filter - contains the filter options like requesttype, targetstatus etc.
     * @param pageSize - Size of the page
     * @param page - current page number
     * @return ModelAndView Object
     */
    @GetMapping("/")
    public ModelAndView homepage(Model model,
            @ModelAttribute("filter") RequestFilter filter,
            @RequestParam("pageSize") Optional<Integer> pageSize,
            @RequestParam("page") Optional<Integer> page) {

        model.addAttribute("requestTypes", monitor.getRequestTypes());
        model.addAttribute("targetStates", monitor.getTargetStates());
        model.addAttribute("version", monitor.getVersion());

        ModelAndView modelAndView = new ModelAndView("index");
        //
        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(monitor.getInitialPageSize());
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? monitor.getInitialPage() : page.get() - 1;

        Page<Request> requestlist =
                requestService.findAll(filter, PageRequest.of(evalPage, evalPageSize, Sort.by("id").descending()));
        PagerModel pager = new PagerModel(requestlist.getTotalPages(),
                requestlist.getNumber(), monitor.getButtonsToShow());
        // add request
        modelAndView.addObject("requestlist", requestlist);
        // evaluate page size
        modelAndView.addObject("selectedPageSize", evalPageSize);
        // add page sizes
        modelAndView.addObject("pageSizes", monitor.getPageSizes());
        // add pager
        modelAndView.addObject("pager", pager);

        //set the total record count
        model.addAttribute("countRequests", requestlist.getTotalElements());

        return modelAndView;
    }

    /**
     * Get the request/forwardrequest message.
     *
     * @param id - for which record id the message has to be retrieved
     * @param msgType - Request, Forward or Response
     * @return String - the message
     */
    @RequestMapping(value = "/getMessage", method = RequestMethod.GET, produces = { "text/plain" })
    @ResponseBody
    public String getMessage(@RequestParam(value = "requestId") Integer id, @RequestParam("msgType") String msgType) {
        if (msgType.equals("Request"))
            return requestService.getRequestDocById(id);
        else if (msgType.equals("Forward"))
            return requestService.getForwardDocById(id);
        else if (msgType.equals("Response"))
            return requestService.getResponseDocById(id);

        return "Error. Unknown message type: " + msgType;
    }

    /**
     * Get the file from RequestEvent.
     *
     * @param id - Record id
     * @param response - HttpServletResponse
     * @throws IOException . during file operations
     * @throws InterruptedException - in case of any interruption.
     */
    @RequestMapping(value = "/events", method = RequestMethod.GET, produces = { "text/plain" })
    @ResponseBody
    public void showEvents(@RequestParam(value = "requestId") Integer id, HttpServletResponse response) throws IOException, InterruptedException {
        log.info("Getting events for request: " + id);

        String allMessages = "";
        String message = "";
        ServletOutputStream Ostream = response.getOutputStream();

        List<RequestEvent> list = requestService.showEvents(id);
        if (list.isEmpty() || (list.size() == 1 && list.get(0) == null))
        //return "No Events found".getBytes();
        {
            Ostream.print("No events found");
            response.flushBuffer();
        } else {
            for (int i = 0; i < list.size(); i++) {
                RequestEvent event = list.get(i);

                String temp = event.toString() + System.getProperty("line.separator");
                allMessages = allMessages.concat(temp);
                Ostream.print(temp);
                response.flushBuffer();
                response.setStatus(SC_OK);
            }
        }
    }

    /**
     * Method for downloading the file.
     *
     * @param session - current user session
     * @param response - HttpServletResponse
     * @param id - Record id
     * @param msgType - Request, Forward or response message
     */
    @RequestMapping(value = "/getMessageAsFile", method = RequestMethod.GET, produces = { "application/xml" })
    @ResponseBody
    public void getMessageAsFile(HttpSession session, HttpServletResponse response, @RequestParam(value = "requestId") Integer id,
            @RequestParam("msgType") String msgType) {

        Request tempRequest = requestService.getRequestById(id);

        //response.setHeader("Content-disposition", "attachment;filename=" + id.toString().concat("_" + msgType+"_").concat(new Timestamp(System.currentTimeMillis()).toString()));
        response.setHeader("Content-disposition",
                "attachment;filename=" + id.toString().concat("_" + msgType + "_").concat(tempRequest.getRequest_date().toString()) + ".xml");
        response.setContentType("application/xml");

        try {
            BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
            int len;

            //String xmlText = "this is a " + msgType + " xml file";
            String xmlText = "";
            if (msgType.equals("Request"))
                xmlText = requestService.getRequestDocById(id);
            else if (msgType.equals("Forward"))
                xmlText = requestService.getForwardDocById(id);
            else if (msgType.equals("Response"))
                xmlText = requestService.getResponseDocById(id);
            byte[] buf = new byte[xmlText.getBytes().length];
            buf = xmlText.getBytes();
            len = buf.length;
            // while ((len = is.read(buf)) > 0)
            {
                outs.write(buf, 0, len);
            }
            outs.close();

        } catch (IOException e) {
            //logger.error("Error ModelAndView.viewMain - IOException : " + e.toString() + " -- " + e.getStackTrace()[0].toString());
            //return null;
        }
        //return "Error. Could not find document";
    }

    /**
     * Added to remove page and pageSize filters from the URL for avoiding duplication.
     * Called from the request_list.html
     *
     * @return - special function called directly from HTML file.
     */
    @Bean
    public Function<String, String> currentUrlWithoutParam() {
        return param -> ServletUriComponentsBuilder.fromCurrentRequest().replaceQueryParam(param).replaceQueryParam("pageSize").toUriString();
    }
}
