package com.getinge.cpq.interfacemonitor.InitialDataLoad;

import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import com.getinge.cpq.interfacemonitor.repositories.RequestEventRepository;
import com.getinge.cpq.interfacemonitor.repositories.RequestRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Component
public class RequestsLoader //implements ApplicationListener<ContextRefreshedEvent>
{

    private RequestRepository requestRepository;
    private RequestEventRepository eventRepository;
    private Logger log = LoggerFactory.getLogger(RequestsLoader.class);

    @Autowired
    public void setRequestRepository(RequestRepository requestRepository)
    {
        this.requestRepository = requestRepository;
    }

    @Autowired
    public void setRequestEventRepository(RequestEventRepository eventRepository)
    {
        this.eventRepository = eventRepository;
    }

   /* @Override
    public void onApplicationEvent(ContextRefreshedEvent event)
    {

        Request request;
        RequestEvent Reqevent;

        for(int i=1; i<=100;i++)
        {
            request = new Request();
            request.setRequest_type("MATMAS");
            request.setName("Added via RequestLoader");
            request.setIntegration_id("PROD"+i);
            request.setRequest_status("Received");
            if(i>1)
            {
                request.setRequest("<xml>Request data...</xml>");
                request.setRequest_date(new Timestamp(System.currentTimeMillis()));
            }
            if(i>2)
            {
                request.setForward_request("<xml>Forward Request data...</xml>");
                request.setForward_request_date(new Timestamp(System.currentTimeMillis()));
            }
            if(i>3)
            {
                request.setForward_response("<xml>Forward Response data...</xml>");
                request.setForward_response_date(new Timestamp(System.currentTimeMillis()));
            }

            request.setTarget_status("Not Processed");
            requestRepository.save(request);
            log.info("Saved Request: " + request.toString());
        }

        Reqevent = new RequestEvent();
        Reqevent.setRequest_id(100);
        Reqevent.setMessage("KAPUT");
        Reqevent.setEvent_timestamp(new Timestamp(System.currentTimeMillis()));
        eventRepository.save(Reqevent);

        Reqevent = new RequestEvent();
        Reqevent.setRequest_id(100);
        Reqevent.setMessage("KAPUT2");
        Reqevent.setEvent_timestamp(new Timestamp(System.currentTimeMillis()));
        eventRepository.save(Reqevent);
        log.info("Saved Request Event:" + event.toString());

    }*/
}

