package com.getinge.cpq.interfacemonitor.services;

import static com.getinge.cpq.interfacemonitor.util.RequestSpecification.requestTypeFilter;
import static com.getinge.cpq.interfacemonitor.util.RequestSpecification.searchFilter;
import static com.getinge.cpq.interfacemonitor.util.RequestSpecification.targetStateFilter;
import static org.springframework.data.jpa.domain.Specification.where;

import java.util.List;

import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import com.getinge.cpq.interfacemonitor.entities.RequestFilter;
import com.getinge.cpq.interfacemonitor.entities.TransferVO;
import com.getinge.cpq.interfacemonitor.repositories.RequestEventRepository;
import com.getinge.cpq.interfacemonitor.repositories.RequestRepository;
import com.getinge.cpq.interfacemonitor.util.RequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * Request Service implementation class.
 */
@Service
public class RequestServiceImp implements RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestEventRepository requestEventRepository;

    @Autowired
    private RequestBuilder requestBuilder;

    @Override
    public Request getRequestById(Integer id) {
        return requestRepository.findById(id).get();
    }

    @Override
    public String getRequestDocById(Integer id) {
        return requestRepository.getRequestDocById(id);
    }

    @Override
    public String getForwardDocById(Integer id) {
        return requestRepository.getForwardDocById(id);
    }

    @Override
    public String getResponseDocById(Integer id) {
        return requestRepository.getResponseDocById(id);
    }

    @Override
    public List<RequestEvent> showEvents(Integer id) {
        return requestRepository.getEventsById(id);
    }

    @Override
    public Page<Request> findAll(RequestFilter filter, PageRequest pageRequest) {
        return requestRepository.findAll(where(requestTypeFilter(filter.getRequest_type()).and
                (targetStateFilter(filter.getTarget_state())).and
                (searchFilter(filter.getSearch()))), pageRequest);
    }

    @Override
    public void createRequestAndRequestEvent(TransferVO transferVO,
            String requestType) {
        Request request = null;
        boolean createRequestEvent = false;
        // check if there was any exception
        if (transferVO.getException() != null) {
            // requestEvent should capture the error.
            createRequestEvent = true;
            request = requestBuilder.setUpRequestForInsertWithError(transferVO, requestType);
        } else {
            //No Problem. Service request and service response both available. Simple Insert is needed.
            request = requestBuilder.setUpRequestForInsert(transferVO, requestType);
        }
        if (request != null) {
            request = requestRepository.save(request);
            if (createRequestEvent) {
                RequestEvent requestEvent = requestBuilder.setUpRequestEvent(request, transferVO.getException());
                requestEventRepository.save(requestEvent);
            }
        }
    }

}
