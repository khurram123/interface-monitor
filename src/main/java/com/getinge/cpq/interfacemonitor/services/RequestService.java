package com.getinge.cpq.interfacemonitor.services;

import java.util.List;

import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import com.getinge.cpq.interfacemonitor.entities.RequestFilter;
import com.getinge.cpq.interfacemonitor.entities.TransferVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.Nullable;

/**
 * Service class that communicates with the Repository.
 */
public interface RequestService {

    /**
     * Method to get the Request Object by request id.
     *
     * @param id - requestId
     * @return - Reqiest Object.
     */
    Request getRequestById(Integer id);

    /**
     * To retrieve the request field by id.
     *
     * @param id - requestId
     * @return request field
     */
    String getRequestDocById(Integer id);

    /**
     * To retrieve the forward field by id.
     *
     * @param id - requestId
     * @return forward field
     */
    String getForwardDocById(Integer id);

    /**
     * To retrieve the resposne field by id.
     *
     * @param id - requestId
     * @return forward response field
     */
    String getResponseDocById(Integer id);

    /**
     * Retrieve the RequestEvent Object.
     *
     * @param id - requestId
     * @return List of RequestEvents
     */
    List<RequestEvent> showEvents(Integer id);

    /**
     * Method to get all the request records if no filter is passed.
     *
     * @param spec - Spec is the filter
     * @param pageRequest - contains the page settings
     * @return - Pages which contains the Request list.
     */
    Page<Request> findAll(@Nullable RequestFilter spec, PageRequest pageRequest);

    /**
     * Method to create the Request and RequestEvent received from cpq-sales-embedded.
     *
     * @param transferVO - Object received from cpq-sales-embedded system
     * @param requestType - Online Pricing or Quote to SAP
     */
    void createRequestAndRequestEvent(TransferVO transferVO, String requestType);

}
