package com.getinge.cpq.interfacemonitor.util;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import com.getinge.cpq.interfacemonitor.entities.TransferVO;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Helper class to help the framing of request and RequestEvent objects.
 */
@Component
public class RequestBuilder {

    private static final String REQUEST_STATUS_QUEUED = "Queued";
    private static final String TARGET_STATUS_PROCESSED = "Processed";
    private static final String TARGET_STATUS_ERROR = "Error";

    @Value("${monitor.timeZoneId}")
    private String timeZoneId;

    /**
     * Transforms the transferVO to Request object.
     *
     * @param transferVO - from cpq-sales-embedded.
     * @param requestType - Online Pricing or Quote to SAP.
     * @return - Request Object.
     */
    public Request setUpRequestForInsert(TransferVO transferVO, String requestType) {
        Request request = new Request();
        request.setRequest(transferVO.getServiceRequest());
        request.setRequest_type(requestType);
        request.setBuildVersion(transferVO.getBuildVersion());
        request.setIntegration(getIntegrationField(transferVO.getIntegration()));
        request.setRequest_date(getCurrentDatePlusSeconds(0));
        request.setRequest_status(REQUEST_STATUS_QUEUED);
        request.setTarget_status(TARGET_STATUS_PROCESSED);
        if (transferVO.getServiceResponse() != null) {
            request.setForward_request(transferVO.getServiceResponse());
            request.setForward_request_date(getCurrentDatePlusSeconds(1));
        }
        if (transferVO.getTransmittedData() != null) {
            request.setForward_response(transferVO.getTransmittedData());
            request.setForward_response_date(getCurrentDatePlusSeconds(2));
        }
        return request;
    }

    /**
     * Transforms the transferVO to Request object in case of an exception.
     *
     * @param transferVO - from cpq-sales-embedded along with the exception.
     * @param requestType - Online Pricing or Quote to SAP.
     * @return - Request Object.
     */
    public Request setUpRequestForInsertWithError(TransferVO transferVO, String requestType) {
        Request request = new Request();
        request.setRequest_type(requestType);
        request.setIntegration(getIntegrationField(transferVO.getIntegration()));
        request.setRequest_date(getCurrentDatePlusSeconds(0));
        request.setRequest_status(REQUEST_STATUS_QUEUED);
        request.setTarget_status(TARGET_STATUS_ERROR);
        request.setBuildVersion(transferVO.getBuildVersion());

        if (transferVO.getServiceRequest() == null) {
            // problem while framing the service request.
            // Here we don't have any details related to the Service Request.
            // Only the error message can be captured.
            request.setRequest("Service Request cannot be retrieved.");
        } else {
            request.setRequest(transferVO.getServiceRequest());
            request.setForward_request_date(getCurrentDatePlusSeconds(1));
            if (transferVO.getServiceResponse() == null) {
                request.setForward_request("Service Response cannot be retrieved.");
            } else {
                request.setForward_request(transferVO.getServiceResponse());
                request.setForward_response(transferVO.getTransmittedData());
                request.setForward_response_date(getCurrentDatePlusSeconds(2));
            }
        }
        return request;
    }

    /**
     * Creates the RequestEvent Object.
     *
     * @param request - primary Object
     * @param exception - Exception from cpq-sales-embedded
     * @return - RequestEvent Object
     */
    public RequestEvent setUpRequestEvent(Request request, Exception exception) {
        RequestEvent requestEvent = new RequestEvent();
        requestEvent.setRequest_id(request.getId());
        requestEvent.setEvent_timestamp(getCurrentDatePlusSeconds(2));
        requestEvent.setMessage(ExceptionUtils.getStackTrace(exception));
        requestEvent.setBuildVersion(request.getBuildVersion());
        return requestEvent;
    }

    /**
     * Integration must be set and cannot be NULL.
     * If it is NULL, data will not be retrieved in the Interface Monitor.
     *
     * @param input - Integration field from cpq-embedded-sales.
     * @return Integration value.
     */
    private String getIntegrationField(String input) {
        return (input != null ? input : "");
    }

    private Timestamp getCurrentDatePlusSeconds(long seconds) {
        ZonedDateTime now = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of(timeZoneId));
        return Timestamp.valueOf(now.toLocalDateTime().plusSeconds(seconds));
    }
}
