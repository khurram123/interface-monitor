package com.getinge.cpq.interfacemonitor.util;

import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestFilter;
import com.getinge.cpq.interfacemonitor.entities.Request_;
import org.springframework.data.jpa.domain.Specification;

/**
 * Specification class to frame the Request filter criteria.
 */
public class RequestSpecification {

    private RequestFilter criteria;

    public RequestSpecification(RequestFilter criteria) {
        this.criteria = criteria;
    }

    /**
     * Frames the filter on the basis of requestType.
     *
     * @param requestType - MATMAS or Online Pricing etc.
     * @return - Specification
     */
    public static Specification<Request> requestTypeFilter(String requestType) {
        return (root, query, cb) -> {
            String containsLikePattern = getContainsLikePattern(requestType);
            return cb.like(cb.lower(root.<String>get(Request_.request_type)), containsLikePattern);
        };
    }

    /**
     * Frames the filter on the basis of targetStatus.
     *
     * @param targetStatus - Processed, Error etc.
     * @return - Specification
     */
    public static Specification<Request> targetStateFilter(String targetStatus) {
        return (root, query, cb) -> {
            String containsLikePattern = getContainsLikePattern(targetStatus);
            return cb.like(cb.lower(root.<String>get(Request_.target_status)), containsLikePattern);
        };

    }

    /**
     * Frames the filter based on searchText.
     *
     * @param searchText - integration field
     * @return - Specification
     */
    public static Specification<Request> searchFilter(String searchText) {
        return (root, query, cb) -> {
            String containsLikePattern = getContainsLikePattern(searchText);
            return cb.like(cb.lower(root.<String>get(Request_.integration)), "%" + containsLikePattern + "%");
        };
    }

    /**
     * If search term is empty, returns %% to include all records.
     * Else returns the searchTerm in lowerCase
     *
     * @param searchTerm - value to be used in the where class in the Specification filter.
     * @return - String.
     */
    private static String getContainsLikePattern(String searchTerm) {
        if (searchTerm == null
                || searchTerm.isEmpty()
                || searchTerm.equalsIgnoreCase("All target states")
                || searchTerm.equalsIgnoreCase("All request types")) {
            return "%";
        } else {
            return searchTerm.toLowerCase();
        }
    }

}