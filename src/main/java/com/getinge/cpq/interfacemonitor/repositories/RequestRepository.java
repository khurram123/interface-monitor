package com.getinge.cpq.interfacemonitor.repositories;

import java.util.List;

import com.getinge.cpq.interfacemonitor.entities.Request;
import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring CRUD repository.
 * Inherits JPASpecificationExecutor to implement query with multiple filters.
 * Inherits PagingAndSortingRepository for paging and sorting implementation.
 */
@Repository
public interface RequestRepository extends CrudRepository<Request, Integer>, JpaSpecificationExecutor<Request>, PagingAndSortingRepository<Request, Integer> {

    /**
     * Native Query to retrieve the request by id.
     *
     * @param id - requestId
     * @return request field
     */
    @Query(name = "Request.getRequest", nativeQuery = true)
    public String getRequestDocById(Integer id);

    /**
     * Native Query to retrieve the forward String by id.
     *
     * @param id - requestId
     * @return forward field
     */
    @Query(name = "Request.getForward", nativeQuery = true)
    public String getForwardDocById(Integer id);

    /**
     * Native Query to retrieve the Response String by id.
     *
     * @param id - requestId
     * @return response field
     */
    @Query(name = "Request.getResponse", nativeQuery = true)
    public String getResponseDocById(Integer id);

    /**
     * Native Query to retrieve the RequestEvent Object.
     *
     * @param id - requestId
     * @return List of RequestEvents
     */
    @Query(name = "Request.GetAllEvents")
    public List<RequestEvent> getEventsById(Integer id);

}
