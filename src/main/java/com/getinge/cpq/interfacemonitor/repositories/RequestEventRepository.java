package com.getinge.cpq.interfacemonitor.repositories;

import com.getinge.cpq.interfacemonitor.entities.RequestEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring repository to inherit CRUD methods like save().
 */
@Repository
public interface RequestEventRepository extends CrudRepository<RequestEvent, Integer> {

}
