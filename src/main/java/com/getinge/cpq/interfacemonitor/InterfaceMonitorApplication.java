package com.getinge.cpq.interfacemonitor;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Main Spring Boot class.
 */
@SpringBootApplication
public class InterfaceMonitorApplication extends SpringBootServletInitializer {

    @Value("${monitor.timeZone}")
    private String timeZone;

    /**
     * Main function. Will be the first function called when the application starts.
     *
     * @param args - defaults args.
     */
    public static void main(String[] args) {
        SpringApplication.run(InterfaceMonitorApplication.class, args);
    }

    @Override
    /**
     * For configuring the Spring Application builder.
     */
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(InterfaceMonitorApplication.class);
    }

    /**
     * Setting the timezone for the application.
     */
    @PostConstruct
    public void started() {
        TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
    }
}
