package com.getinge.cpq.interfacemonitor.configuration;

//import org.h2.server.web.WebServlet;

import org.springframework.context.annotation.Configuration;

/* The following Spring Configuration declares the servlet wrapper for the H2 database console and maps it to the path of /console. */

/**
 * Spring Web Configuration class.
 */
@Configuration
public class WebConfiguration {
    //    @Bean
    //    ServletRegistrationBean h2servletRegistration()
    //    {
    //        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
    //        registrationBean.addUrlMappings("/console/*");
    //        return registrationBean;
    //    }
}