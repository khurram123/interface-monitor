package com.getinge.cpq.interfacemonitor.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configuration class.
 */
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.getinge.cpq.interfacemonitor.entities"})
@EnableJpaRepositories(basePackages = {"com.getinge.cpq.interfacemonitor.repositories"})
@EnableTransactionManagement
public class RepositoryConfiguration
{
}