package com.getinge.cpq.interfacemonitor.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Spring Configuration class.
 */
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/").permitAll().and().authorizeRequests().antMatchers("/console/**").permitAll();

        httpSecurity.csrf().disable();
        httpSecurity.headers().frameOptions().disable();
    }

    /*
    To enable access to the H2 database console under Spring Security you need to change three things:

    Allow all access to the url path /console/*.
    Disable CRSF (Cross-Site Request Forgery). By default, Spring Security will protect against CRSF attacks.
    Since the H2 database console runs inside a frame, you need to enable this in in Spring Security.
    The following Spring Security Configuration will:

    Allow all requests to the root url (“/”)
    Allow all requests to the H2 database console url (“/console/*”)
    Disable CSRF protection
    Disable X-Frame-Options in Spring Security
    CAUTION: This is not a Spring Security Configuration that you would want to use for a production website. These settings are only to support development of a Spring Boot web application and enable access to the H2 database console. I cannot think of an example where you’d actually want the H2 database console exposed on a production database.
    */
}