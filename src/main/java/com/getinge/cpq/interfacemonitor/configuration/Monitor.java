package com.getinge.cpq.interfacemonitor.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Class to monitor the filter and page settings.
 */
@ConfigurationProperties(prefix = "monitor")
@Component
public class Monitor {

    Integer initialPageSize;
    Integer[] pageSizes;
    Integer buttonsToShow;
    String[] requestTypes;
    String[] targetStates;
    String version;
    Integer initialPage;

    public Monitor() {
    }

    public Integer getInitialPageSize() {
        return initialPageSize;
    }

    public void setInitialPageSize(Integer initialPageSize) {
        this.initialPageSize = initialPageSize;
    }

    public Integer getInitialPage() {
        return initialPage;
    }

    public void setInitialPage(Integer initialPage) {
        this.initialPage = initialPage;
    }

    public Integer[] getPageSizes() {
        return pageSizes;
    }

    public void setPageSizes(Integer[] pageSizes) {
        this.pageSizes = pageSizes;
    }

    public Integer getButtonsToShow() {
        return buttonsToShow;
    }

    public void setButtonsToShow(Integer buttonsToShow) {
        this.buttonsToShow = buttonsToShow;
    }

    public String[] getRequestTypes() {
        return requestTypes;
    }

    public void setRequestTypes(String[] requestTypes) {
        this.requestTypes = requestTypes;
    }

    public String[] getTargetStates() {
        return targetStates;
    }

    public void setTargetStates(String[] targetStates) {
        this.targetStates = targetStates;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
