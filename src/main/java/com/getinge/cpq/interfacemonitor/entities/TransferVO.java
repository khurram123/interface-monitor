package com.getinge.cpq.interfacemonitor.entities;

/**
 * Simple POJO class.
 * Used to retrieve the Request Object from cpq-sales-embedded.
 */
public class TransferVO {

    private String serviceRequest;

    private String serviceResponse;

    private Exception exception;

    private String integration;

    private String buildVersion;

    private String transmittedData;

    public String getServiceRequest() {
        return serviceRequest;
    }

    public String getServiceResponse() {
        return serviceResponse;
    }

    public Exception getException() {
        return exception;
    }

    public String getIntegration() {
        return integration;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public String getTransmittedData() {
        return transmittedData;
    }
}

