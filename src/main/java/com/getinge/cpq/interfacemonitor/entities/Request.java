package com.getinge.cpq.interfacemonitor.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * POJO class used as an entity.
 * Also contains some Named queries.
 */
@Entity
@Table(name = "cpq_intfc_requests")
@NamedQueries
        ({
                @NamedQuery(name = "Request.GetAllEvents",
                        query = "SELECT e FROM Request r left outer join RequestEvent e on r.id = e.request_id where r.id = ? order by e.id asc"),
        })

@SqlResultSetMappings
        ({
                @SqlResultSetMapping(name = "countMapping", columns = { @ColumnResult(name = "requestCount") }),
                @SqlResultSetMapping(name = "getRequest", columns = { @ColumnResult(name = "request") }),
                @SqlResultSetMapping(name = "getForward", columns = { @ColumnResult(name = "forward_request") }),
                @SqlResultSetMapping(name = "getResponse", columns = { @ColumnResult(name = "forward_response") }),
                @SqlResultSetMapping(name = "getAllRequestsWithoutXML", entities = { @EntityResult(entityClass = Request.class,
                        fields = { @FieldResult(name = "id", column = "id"),
                                @FieldResult(name = "forward_request_date", column = "forward_request_date"),
                                @FieldResult(name = "forward_response_date", column = "forward_response_date"),
                                @FieldResult(name = "integration", column = "integration"),
                                @FieldResult(name = "name", column = "name"),
                                @FieldResult(name = "request_date", column = "request_date"),
                                @FieldResult(name = "request_status", column = "request_status"),
                                @FieldResult(name = "request_type", column = "request_type"),
                                @FieldResult(name = "target_status", column = "target_status")
                        })
                }),
        })

@NamedNativeQueries
        ({
                @NamedNativeQuery(name = "Request.getRequest",
                        query = "SELECT r.request as request FROM cpq_intfc_requests r where id = ?",
                        resultSetMapping = "getRequest"),
                @NamedNativeQuery(name = "Request.getForward",
                        query = "SELECT r.forward_request as forward_request FROM cpq_intfc_requests r where id = ?",
                        resultSetMapping = "getForward"),
                @NamedNativeQuery(name = "Request.getResponse",
                        query = "SELECT r.forward_response as forward_response FROM cpq_intfc_requests r where id = ?",
                        resultSetMapping = "getResponse"),
        })
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Version
    private Integer version;

    private String request_type;
    private String request_status;
    private String integration;

    @Column(columnDefinition = "VARCHAR(max)")
    private String request;
    private Timestamp request_date;

    @Column(columnDefinition = "VARCHAR(max)")
    private String forward_request;
    private Timestamp forward_request_date;

    @Column(columnDefinition = "VARCHAR(max)")
    private String forward_response;
    private Timestamp forward_response_date;

    private String name;
    private String lookup_status;

    private String target_status;

    @Column(name = "buildversion")
    private String buildVersion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getIntegration() {
        return integration;
    }

    public void setIntegration(String integration) {
        this.integration = integration;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Timestamp getRequest_date() {
        return request_date;
    }

    public void setRequest_date(Timestamp request_date) {
        this.request_date = request_date;
    }

    public String getForward_request() {
        return forward_request;
    }

    public void setForward_request(String forward_request) {
        this.forward_request = forward_request;
    }

    public Timestamp getForward_request_date() {
        return forward_request_date;
    }

    public void setForward_request_date(Timestamp forward_request_date) {
        this.forward_request_date = forward_request_date;
    }

    public String getForward_response() {
        return forward_response;
    }

    public void setForward_response(String forward_response) {
        this.forward_response = forward_response;
    }

    public Timestamp getForward_response_date() {
        return forward_response_date;
    }

    public void setForward_response_date(Timestamp forward_response_date) {
        this.forward_response_date = forward_response_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLookup_status() {
        return lookup_status;
    }

    public void setLookup_status(String lookup_status) {
        this.lookup_status = lookup_status;
    }

    public String getTarget_status() {
        return target_status;
    }

    public void setTarget_status(String target_status) {
        this.target_status = target_status;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }
}
