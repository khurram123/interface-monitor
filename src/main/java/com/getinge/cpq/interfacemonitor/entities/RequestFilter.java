package com.getinge.cpq.interfacemonitor.entities;

/**
 * Contains the filters from HTML page.
 */
public class RequestFilter {

    String search;
    String request_type;
    String target_state;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public String getTarget_state() {
        return target_state;
    }

    public void setTarget_state(String target_state) {
        this.target_state = target_state;
    }

}
