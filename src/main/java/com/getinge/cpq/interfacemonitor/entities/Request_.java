package com.getinge.cpq.interfacemonitor.entities;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.getinge.cpq.interfacemonitor.util.RequestSpecification;

/**
 * Special Metamodel class used by RequestSpecification.
 *
 * @see RequestSpecification
 */
@StaticMetamodel(Request.class)
public class Request_ {

    public static volatile SingularAttribute<Request, Integer> id;
    public static volatile SingularAttribute<Request, String> request_type;
    public static volatile SingularAttribute<Request, String> target_status;
    public static volatile SingularAttribute<Request, String> integration;
}
