package com.getinge.cpq.interfacemonitor.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * POJO class for cpq_intfc_requests_events used an an entity.
 */
@Entity
@Table(name = "cpq_intfc_requests_events")
public class RequestEvent
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Version
    private Integer version;

    private Integer request_id;
    private Timestamp event_timestamp;
    private String message;

    @Column(name = "buildversion")
    private String buildVersion;

    public Integer getRequest_id() {
        return request_id;
    }

    public void setRequest_id(Integer request_id) {
        this.request_id = request_id;
    }

    public Timestamp getEvent_timestamp() {
        return event_timestamp;
    }

    public void setEvent_timestamp(Timestamp event_timestamp) {
        this.event_timestamp = event_timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestEvent{");
        sb.append("request_id=").append(request_id);
        sb.append(", event_timestamp=").append(event_timestamp);
        sb.append(", buildVersion='").append(buildVersion).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
