function downloadMessage(index, msgType) {
    var url = 'getMessageAsFile?requestId=' + index + '&msgType=' + msgType;
    window.open(url);
}

function showMessageDialog(index, msgType) {
    var url = 'getMessage?requestId=' + index + '&msgType=' + msgType;
    window.open(url);
}

function showEvents(index) {
    var url = 'events?requestId=' + index;
    window.open(url);
}
